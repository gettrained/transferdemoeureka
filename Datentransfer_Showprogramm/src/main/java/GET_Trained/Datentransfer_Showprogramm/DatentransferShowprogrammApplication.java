package GET_Trained.Datentransfer_Showprogramm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DatentransferShowprogrammApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatentransferShowprogrammApplication.class, args);
	}
}
